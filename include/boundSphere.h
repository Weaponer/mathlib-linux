#ifndef BOUND_SPHERE_H_INCLUDED
#define BOUND_SPHERE_H_INCLUDED

#include <MTH/vectors.h>

namespace mth
{
    template <typename T>
    class BoundSphere
    {
    public:
        Vector3<T> origin;
        T radius;

        BoundSphere() : origin(0, 0, 0), radius(0)
        {
        }

        BoundSphere(const Vector3<T> &origin, const T &radius) : origin(origin), radius(radius)
        {
        }

        bool checkPointInside(const Vector3<T> & point) const
        {
            return radius * radius >= (point - origin).squareMagnitude();
        }
    };
    typedef BoundSphere<float> boundSphere;
}

#endif