#ifndef BOUNDS_H_INCLUDED
#define BOUNDS_H_INCLUDED

#include <MTH/boundAABB.h>
#include <MTH/boundOBB.h>
#include <MTH/plane.h>
#include <MTH/boundSphere.h>
#include <MTH/closestPoint.h>
#include <cmath>
#include <numeric>
#include <limits>

namespace mth
{
    template <typename T>
    BoundAABB<T> AABB_add_AABB(const BoundAABB<T> &oneAABB, const BoundAABB<T> &twoAABB)
    {
        T minX = std::min(oneAABB.min.x, twoAABB.min.x);
        T minY = std::min(oneAABB.min.y, twoAABB.min.y);
        T minZ = std::min(oneAABB.min.z, twoAABB.min.z);

        T maxX = std::max(oneAABB.max.x, twoAABB.max.x);
        T maxY = std::max(oneAABB.max.y, twoAABB.max.y);
        T maxZ = std::max(oneAABB.max.z, twoAABB.max.z);

        Vector3<T> max = Vector3<T>(maxX, maxY, maxZ);
        Vector3<T> origin = Vector3<T>((Vector3<T>(minX, minY, minZ) + max) / 2);
        Vector3<T> size = max - origin;
        size.x = std::abs(size.x);
        size.y = std::abs(size.y);
        size.z = std::abs(size.z);
        return BoundAABB<T>(origin, size);
    }

    template <typename T>
    BoundAABB<T> generate_AABB_from_points(const size_t &countPoints, const Vector3<T> *points)
    {
        if (countPoints > 0)
        {
            T minX = points[0].x;
            T minY = points[0].y;
            T minZ = points[0].z;

            T maxX = points[0].x;
            T maxY = points[0].y;
            T maxZ = points[0].z;
            for (int i = 1; i < countPoints; i++)
            {
                if (points[i].x < minX)
                {
                    minX = points[i].x;
                }
                else if (points[i].x > maxX)
                {
                    maxX = points[i].x;
                }

                if (points[i].y < minY)
                {
                    minY = points[i].y;
                }
                else if (points[i].y > maxY)
                {
                    maxY = points[i].y;
                }

                if (points[i].z < minZ)
                {
                    minZ = points[i].z;
                }
                else if (points[i].z > maxZ)
                {
                    maxZ = points[i].z;
                }
            }

            Vector3<T> max = Vector3<T>(maxX, maxY, maxZ);
            Vector3<T> min = Vector3<T>(minX, minY, minZ);
            Vector3<T> pos = (max + min) / 2;

            Vector3<T> size = (max - min) / 2;
            size = Vector3<T>(abs(size.x), abs(size.y), abs(size.z));
            return BoundAABB<T>(pos, size);
        }
        else
        {
            return BoundAABB<T>();
        }
    }

    template <typename T>
    BoundSphere<T> Sphere_add_Sphere(const BoundSphere<T> &oneSphere, const BoundSphere<T> &twoSphere)
    {
        Vector3<T> direct = twoSphere.origin - oneSphere.origin;
        T diameter = direct.magnitude() + oneSphere.radius + twoSphere.radius;
        direct.normalise();
        T radius = diameter / 2;
        return BoundSphere<T>(direct * (radius - oneSphere.radius) + oneSphere.origin, radius);
    }

    template <typename T>
    bool test_Sphere_Sphere(const BoundSphere<T> &oneSphere, const BoundSphere<T> &twoSphere)
    {
        float sumRadius = oneSphere.radius + twoSphere.radius;
        return (twoSphere.origin - oneSphere.origin).squareMagnitude() <= (sumRadius * sumRadius);
    }

    template <typename T>
    bool test_Sphere_Plane(const BoundSphere<T> &sphere, const Plane<T> &plane)
    {
        T dist = sphere.origin * plane.normal - plane.dist;
        return dist <= sphere.radius;
    }

    template <typename T>
    bool test_AABB_AABB(const BoundAABB<T> &oneAABB, const BoundAABB<T> &twoAABB)
    {
        if (oneAABB.max.x < twoAABB.min.x || oneAABB.min.x > twoAABB.max.x)
            return false;
        if (oneAABB.max.y < twoAABB.min.y || oneAABB.min.y > twoAABB.max.y)
            return false;
        if (oneAABB.max.z < twoAABB.min.z || oneAABB.min.z > twoAABB.max.z)
            return false;
        return true;
    }

    template <typename T>
    bool test_Segment_AABB(const Vector3<T> &point0, const Vector3<T> &point1, const BoundAABB<T> &aabb)
    {
        Vector3<T> c = aabb.origin;
        Vector3<T> e = aabb.size;

        Vector3<T> m = (point0 + point1) * 0.5;
        Vector3<T> d = point1 - m;
        m = m - c;

        T adx = abs(d.x);
        if(abs(m.x) > e.x + adx) return false;
        T ady = abs(d.y);
        if(abs(m.y > e.y + ady)) return false;
        T adz = abs(d.z);
        if(abs(m.z > e.z + adz)) return false;

        adx += std::numeric_limits<T>::epsilon();
        ady += std::numeric_limits<T>::epsilon();
        adz += std::numeric_limits<T>::epsilon();

        if(abs(m.y * d.z - m.z * d.y) > e.y * adz + e.z * ady) return 0;
        if(abs(m.z * d.x - m.x * d.z) > e.x * adz + e.z * adx) return 0;
        if(abs(m.x * d.y - m.y * d.x) > e.x * ady + e.z * adx) return 0;

        return 1;
    }

    template <typename T>
    bool test_AABB_Sphere(const BoundAABB<T> &AABB, const BoundSphere<T> &sphere)
    {
        T sqDist = sqrDistPointAABB(sphere.origin, AABB);

        return sqDist <= sphere.radius * sphere.radius;
    }

    template <typename T>
    bool test_AABB_Plane(const BoundAABB<T> &b, const Plane<T> &p)
    {
        Vector3<T> c = b.origin;
        Vector3<T> e = b.max - c;

        T r = e[0] * abs(p.normal[0]) + e[1] * abs(p.normal[1]) + e[2] * abs(p.normal[2]);

        T s = c * p.normal - p.dist;
        return s <= r;
    }

    // OBB
    template <typename T>
    bool test_OBB_Sphere(const BoundOBB<T> &OBB, const BoundSphere<T> &sphere)
    {
        Vector3<T> p;

        closestPtPointOBB(sphere.origin, OBB, p);
        Vector3<T> v = p - sphere.origin;
        return (v * v) <= sphere.radius * sphere.radius;
    }

    template <typename T>
    bool test_OBB_Plane(const BoundOBB<T> &b, const Plane<T> &p)
    {
        T r = b.size[0] * abs(p.normal * b.rotate.getVector(0)) +
              b.size[1] * abs(p.normal * b.rotate.getVector(1)) +
              b.size[2] * abs(p.normal * b.rotate.getVector(2));

        T s = b.center * p.normal - p.dist;

        return s <= r;
    }

    template <typename T>
    bool test_OBB_OBB(const BoundOBB<T> &a, const BoundOBB<T> &b)
    {
        T ra;
        T rb;
        Matrix3<T> R;
        Matrix3<T> AbsR;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                R.m[i][j] = a.rotate.getVector(i) * b.rotate.getVector(j);
            }
        }
        Vector3<T> t = b.center - a.center;
        t = Vector3<T>(t * a.rotate.getVector(0), t * a.rotate.getVector(1), t * a.rotate.getVector(2));

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                AbsR.m[i][j] = abs(R.m[i][j]) + std::numeric_limits<T>::epsilon();
            }
        }

        for (int i = 0; i < 3; i++)
        {
            ra = a.size[i];
            rb = b.size[0] * AbsR.m[i][0] + b.size[1] * AbsR.m[i][1] + b.size[2] * AbsR.m[i][2];
            if (abs(t[i]) > ra + rb)
                return false;
        }

        for (int i = 0; i < 3; i++)
        {
            ra = a.size[0] * AbsR.m[0][i] + a.size[1] * AbsR.m[1][i] + a.size[2] * AbsR.m[2][i];
            rb = b.size[i];
            if (abs(t[0] * R.m[0][i] + t[1] * R.m[1][i] + t[2] * R.m[2][i]) > ra + rb)
                return false;
        }

        ra = a.size[1] * AbsR.m[2][0] + a.size[2] * AbsR.m[1][0];
        rb = b.size[1] * AbsR.m[0][2] + b.size[2] * AbsR.m[0][1];
        if (abs(t[2] * R.m[1][0] - t[1] * R.m[2][0]) > ra + rb)
            return false;

        ra = a.size[1] * AbsR.m[2][1] + a.size[2] * AbsR.m[1][1];
        rb = b.size[0] * AbsR.m[0][2] + b.size[2] * AbsR.m[0][0];
        if (abs(t[2] * R.m[1][1] - t[1] * R.m[2][1]) > ra + rb)
            return false;

        ra = a.size[1] * AbsR.m[2][2] + a.size[2] * AbsR.m[1][2];
        rb = b.size[0] * AbsR.m[0][1] + b.size[1] * AbsR.m[0][0];
        if (abs(t[2] * R.m[1][2] - t[1] * R.m[2][2]) > ra + rb)
            return false;

        ra = a.size[0] * AbsR.m[2][0] + a.size[2] * AbsR.m[0][0];
        rb = b.size[1] * AbsR.m[1][2] + b.size[2] * AbsR.m[1][1];
        if (abs(t[0] * R.m[2][0] - t[2] * R.m[0][0]) > ra + rb)
            return false;

        ///
        ra = a.size[0] * AbsR.m[2][1] + a.size[2] * AbsR.m[0][1];
        rb = b.size[0] * AbsR.m[1][2] + b.size[2] * AbsR.m[1][0];
        if (abs(t[0] * R.m[2][1] - t[2] * R.m[0][1]) > ra + rb)
            return false;

        ra = a.size[0] * AbsR.m[2][2] + a.size[2] * AbsR.m[0][2];
        rb = b.size[0] * AbsR.m[1][1] + b.size[1] * AbsR.m[1][0];
        if (abs(t[0] * R.m[2][2] - t[2] * R.m[0][2]) > ra + rb)
            return false;

        ra = a.size[0] * AbsR.m[1][0] + a.size[1] * AbsR.m[0][0];
        rb = b.size[1] * AbsR.m[2][2] + b.size[2] * AbsR.m[2][1];
        if (abs(t[1] * R.m[0][0] - t[0] * R.m[1][0]) > ra + rb)
            return false;

        ra = a.size[0] * AbsR.m[1][1] + a.size[1] * AbsR.m[0][1];
        rb = b.size[0] * AbsR.m[2][2] + b.size[2] * AbsR.m[2][0];
        if (abs(t[1] * R.m[0][1] - t[0] * R.m[1][1]) > ra + rb)
            return false;

        ra = a.size[0] * AbsR.m[1][2] + a.size[1] * AbsR.m[0][2];
        rb = b.size[0] * AbsR.m[2][1] + b.size[1] * AbsR.m[2][0];
        if (abs(t[1] * R.m[0][2] - t[0] * R.m[1][2]) > ra + rb)
            return false;

        return true;
    }
}

#endif