#ifndef BOUND_AABB_H_INCLUDED
#define BOUND_AABB_H_INCLUDED

#include <MTH/vectors.h>
#include <MTH/quaternion.h>
#include <array>

namespace mth
{
    template <typename T>
    class BoundAABB
    {
    public:
        Vector3<T> origin;
        Vector3<T> size;

        Vector3<T> min;
        Vector3<T> max;

        BoundAABB() : origin(0, 0, 0), size(1, 1, 1), min(-size), max(size)
        {
        }

        BoundAABB(const Vector3<T> &origin, const Vector3<T> &size) : origin(origin), size(size), min(origin - size), max(origin + size)
        {
        }

        BoundAABB(const BoundAABB<T> &bound) : origin(bound.origin), size(bound.size), min(origin - size), max(origin + size)
        {
        }

        void calculateMinMax()
        {
            min = origin - size;
            max = origin + size;
        }

        void setMinMax(const Vector3<T> &_min, const Vector3<T> &_max)
        {
            min = _min;
            max = _max;
            size = max - min;
            size.x = abs(size.x);
            size.y = abs(size.y);
            size.z = abs(size.z);
            size = size / 2.0f;
            origin = min + size;
        }

        void resizeForMatrix4(const Matrix4<T> &mat)
        {
            std::array<Vector3<T>, 8> points;

            points[0] = mat.transformNoMove(Vector3<T>(-size.x, size.y, size.z));
            points[1] = mat.transformNoMove(Vector3<T>(size.x, size.y, size.z));
            points[2] = mat.transformNoMove(Vector3<T>(size.x, size.y, -size.z));
            points[3] = mat.transformNoMove(Vector3<T>(-size.x, size.y, -size.z));
            points[4] = mat.transformNoMove(Vector3<T>(-size.x, -size.y, size.z));
            points[5] = mat.transformNoMove(Vector3<T>(size.x, -size.y, size.z));
            points[6] = mat.transformNoMove(Vector3<T>(size.x, -size.y, -size.z));
            points[7] = mat.transformNoMove(Vector3<T>(-size.x, -size.y, -size.z));

            for (int i = 0; i < 8; i++)
            {
                T x = points[i].x;
                if (x > size.x)
                {
                    size.x = x;
                }
                T y = points[i].y;
                if (y > size.y)
                {
                    size.y = y;
                }
                T z = points[i].z;
                if (z > size.z)
                {
                    size.z = z;
                }
            }

            origin = mat.transform(origin);
            calculateMinMax();
        }

        void resizeRotateOnly(const Matrix4<T> &mat)
        {
            std::array<Vector3<T>, 8> points;

            points[0] = mat.transformNoMove(Vector3<T>(-size.x, size.y, size.z));
            points[1] = mat.transformNoMove(Vector3<T>(size.x, size.y, size.z));
            points[2] = mat.transformNoMove(Vector3<T>(size.x, size.y, -size.z));
            points[3] = mat.transformNoMove(Vector3<T>(-size.x, size.y, -size.z));
            points[4] = mat.transformNoMove(Vector3<T>(-size.x, -size.y, size.z));
            points[5] = mat.transformNoMove(Vector3<T>(size.x, -size.y, size.z));
            points[6] = mat.transformNoMove(Vector3<T>(size.x, -size.y, -size.z));
            points[7] = mat.transformNoMove(Vector3<T>(-size.x, -size.y, -size.z));

            for (int i = 0; i < 8; i++)
            {
                T x = points[i].x;
                if (x > size.x)
                {
                    size.x = x;
                }
                T y = points[i].y;
                if (y > size.y)
                {
                    size.y = y;
                }
                T z = points[i].z;
                if (z > size.z)
                {
                    size.z = z;
                }
            }

            calculateMinMax();
        }

        void resizeForRotate(const Quaternion<T> &rot)
        {
            resizeForMatrix4(rot.getMatrix());
        }

        bool checkPointInside(const Vector3<T> &point) const
        {
            Vector3<T> p = point - origin;
            if (abs(p.x) <= size.x && abs(p.y) <= size.y && abs(p.z) <= size.z)
            {
                return true;
            }
            return false;
        }
    };

    typedef BoundAABB<float> boundAABB;
}
#endif