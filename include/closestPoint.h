#ifndef CLOSEST_POINT_H_INCLUDED
#define CLOSEST_POINT_H_INCLUDED

#include <MTH/base.h>
#include <MTH/vectors.h>
#include <MTH/boundAABB.h>
#include <MTH/boundOBB.h>
#include <MTH/matrix.h>
#include <MTH/plane.h>
#include <cmath>
#include <numeric>
#include <limits>

namespace mth
{
    template <typename T>
    void closestPtPointAABB(const Vector3<T> &p, const BoundAABB<T> &b, Vector3<T> &q)
    {
        T v = p.x;
        if (v < b.min.x)
            v = b.min.x;
        if (v > b.max.x)
            v = b.max.x;
        q.x = v;

        v = p.y;
        if (v < b.min.y)
            v = b.min.y;
        if (v > b.max.y)
            v = b.max.y;
        q.y = v;

        v = p.z;
        if (v < b.min.z)
            v = b.min.z;
        if (v > b.max.z)
            v = b.max.z;
        q.z = v;
    }

    template <typename T>
    T sqrDistPointAABB(const Vector3<T> &p, const BoundAABB<T> &b)
    {
        T sqDist = 0.0;
        T v = p.x;
        if (v < b.min.x)
            sqDist += (b.min.x - v) * (b.min.x - v);
        if (v > b.max.x)
            sqDist += (v - b.max.x) * (v - b.max.x);

        v = p.y;
        if (v < b.min.y)
            sqDist += (b.min.y - v) * (b.min.y - v);
        if (v > b.max.y)
            sqDist += (v - b.max.y) * (v - b.max.y);

        v = p.z;
        if (v < b.min.z)
            sqDist += (b.min.z - v) * (b.min.z - v);
        if (v > b.max.z)
            sqDist += (v - b.max.z) * (v - b.max.z);
        return sqDist;
    }

    template <typename T>
    Vector3<T> closestPtPointPlane(const Vector3<T> &q, const Plane<T> &p)
    {
        T t = p.normal * q - p.dist;
        return q - p.normal * t;
    }

    template <typename T>
    T distPointPlane(const Vector3<T> &q, const Plane<T> &p)
    {
        T t = p.normal * q - p.dist;
        return q - t * p.normal;
    }

    template <typename T>
    void closestPtPointSegment(const Vector3<T> &c, const Vector3<T> &a, const Vector3<T> &b, float &t, Vector3<T> &d)
    {
        Vector3<T> ab = b - a;
        t = ((c - a) * (ab)) / (ab * ab);
        if (t < 0.0)
            t = 0.0;
        if (t > 1.0)
            t = 1.0;

        d = a + t * ab;
    }

    template <typename T>
    T sqrDistPointSegment(const Vector3<T> &a, const Vector3<T> &b, const Vector3<T> &c)
    {
        Vector3<T> ab = b - a;
        Vector3<T> ac = c - a;
        Vector3<T> bc = c - b;
        float e = ac * ab;
        if (e <= 0.0)
            return ac * ac;
        float f = ab * ab;
        if (e >= f)
            return bc * bc;
        return (ac * ac) - e * e / f;
    }

    //OBB
    template <typename T>
    void closestPtPointOBB(const Vector3<T> &p, const BoundOBB<T> &b, Vector3<T> &q)
    {
        Vector3<T> d = p - b.center;

        q = b.center;
        for (int i = 0; i < 3; i++)
        {
            Vector3<T> axis = b.rotate.getVector(i);
            T dist = ( d * axis);
            if(dist > b.size[i]) dist = b.size[i];
            if(dist < -b.size[i]) dist = -b.size[i];
            q += axis * dist;
        }
    }

    template <typename T>
    T sqDistPointOBB(const Vector3<T> &p, const BoundOBB<T> &b)
    {
        Vector3<T> closest;
        closestPtPointOBB(p, b, closest);
        T sqDist = (closest - p) * (closest - p);
        return sqDist;
    }

    template <typename T>
    T closestPointSegmentSegment(const Vector3<T> &p1, const Vector3<T> &q1, const Vector3<T> &p2, const Vector3<T> &q2,
                                 T &s, T &t, Vector3<T> &c1, Vector3<T> &c2)
    {
        Vector3<T> d1 = q1 - p1;
        Vector3<T> d2 = q2 - p2;
        Vector3<T> r = p1 - p2;
        T a = d1 * d1;
        T e = d2 * d2;
        T f = d2 * r;

        T epsilon = std::numeric_limits<T>::epsilon();

        if(a <= epsilon && e <= epsilon)
        {
            s = t = 0.0;
            c1 = p1;
            c2 = p2;
            return (c1 - c2) * (c1 - c2);
        }

        if(a <= epsilon)
        {
            s = 0.0;
            t = f / e;
            t = clamp(t, (T)0.0, (T)1.0);
        }
        else
        {
            T c = d1 * r;
            if(e <= epsilon)
            {
                t = 0.0;
                s = clamp(-c / a, (T)0.0, (T)1.0);
            }
            else
            {
                T b = d1 * d2;
                T denom = a * e - b * b;
                if(denom != 0.0)
                {
                    s = clamp((b * f - c * e) / denom, (T)0.0, (T)1.0);
                }
                else
                {
                    s = 0.0;
                }

                t = (b * s + f) / e;

                if(t < 0.0)
                {
                    t = 0.0;
                    s = clamp(-c / a, (T)0.0, (T)1.0);
                }
                else if(t > 1.0)
                {
                    t = 1.0;
                    s = clamp((b - c) / a, (T)0.0, (T)1.0);
                } 
            }
        }
        c1 = p1 + d1 * s;
        c2 = p2 + d2 * t;
        return (c1 - c2) * (c1 - c2); 
    }
}

#endif