#ifndef VECTORS_H_INCLUDED
#define VECTORS_H_INCLUDED
#include <iostream>
#include "MTH/base.h"
#include <math.h>

namespace mth
{
    template <class T>
    class Vector2
    {
    public:
        T x;
        T y;

        Vector2()
        {
            x = 0;
            y = 0;
        }

        Vector2(T x, T y)
        {
            this->x = x;
            this->y = y;
        }

        Vector2(const Vector2<T> &v)
        {
            this->x = v.x;
            this->y = v.y;
        }

        inline void operator=(const Vector2<T> &v)
        {
            x = v.x;
            y = v.y;
        }

        inline bool operator==(const Vector2<T> &v) const
        {
            return (x == v.x && y == v.y);
        }

        inline bool operator!=(const Vector2<T> &v) const
        {
            return !(x == v.x && y == v.y);
        }

        inline bool operator>(const Vector2<T> &v) const
        {
            return (x > v.x && y > v.y);
        }

        inline bool operator<(const Vector2<T> &v) const
        {
            return (x < v.x && y < v.y);
        }

        inline bool operator>=(const Vector2<T> &v) const
        {
            return (x >= v.x && y >= v.y);
        }

        inline bool operator<=(const Vector2<T> &v) const
        {
            return (x <= v.x && y <= v.y);
        }

        inline Vector2<T> operator+(const Vector2<T> &v) const
        {
            return Vector2<T>(x + v.x, y + v.y);
        }

        inline Vector2<T> operator-(const Vector2<T> &v) const
        {
            return Vector2<T>(x - v.x, y - v.y);
        }

        inline Vector2<T> operator-() const
        {
            return Vector2<T>(-x, -y);
        }

        inline void operator+=(const Vector2<T> &v)
        {
            x += v.x;
            y += v.y;
        }

        inline void operator-=(const Vector2<T> &v)
        {
            x -= v.x;
            y -= v.y;
        }

        inline Vector2<T> operator*(const T &v) const
        {
            return Vector2<T>(x * v, y * v);
        }

        inline Vector2<T> operator/(const T &v) const
        {
            return Vector2<T>(x / v, y / v);
        }

        inline void operator*=(const T &v)
        {
            x *= v;
            y *= v;
        }

        inline void operator/=(const T &v)
        {
            x /= v;
            y /= v;
        }

        inline T operator*(const Vector2<T> &v)
        {
            return (x * v.x + y * v.y);
        }

        inline Vector2<T> perpendecular() const
        {
            return Vector2<T>(-y, x);
        }

        inline T magnitude() const
        {
            return std::sqrt(x * x + y * y);
        }

        inline T squareMagnitude() const
        {
            return x * x + y * y;
        }

        inline void normalise()
        {
            T l = magnitude();
            if (l > 0)
            {
                this->operator*=((T)1 / l);
            }
        }

        inline Vector2<T> unit()
        {
            Vector2<T> n = *this;
            n.normalise();
            return n;
        }

        inline void clear()
        {
            x = y = 0;
        }

        inline void invert()
        {
            x = -x;
            y = -y;
        }
    };

    class Vector2Int
    {
    public:
        int x;
        int y;

        Vector2Int()
        {
            x = 0;
            y = 0;
        }

        Vector2Int(int x, int y)
        {
            this->x = x;
            this->y = y;
        }

        Vector2Int(const Vector2Int &v)
        {
            this->x = v.x;
            this->y = v.y;
        }

        inline bool operator==(const Vector2Int &v) const
        {
            return (x == v.x && y == v.y);
        }

        inline bool operator!=(const Vector2Int &v) const
        {
            return !(x == v.x && y == v.y);
        }

        inline bool operator>(const Vector2Int &v) const
        {
            return (x > v.x && y > v.y);
        }

        inline bool operator<(const Vector2Int &v) const
        {
            return (x < v.x && y < v.y);
        }

        inline bool operator>=(const Vector2Int &v) const
        {
            return (x >= v.x && y >= v.y);
        }

        inline bool operator<=(const Vector2Int &v) const
        {
            return (x <= v.x && y <= v.y);
        }

        inline Vector2Int operator+(const Vector2Int &v) const
        {
            return Vector2Int(x + v.x, y + v.y);
        }

        inline Vector2Int operator-(const Vector2Int &v) const
        {
            return Vector2Int(x - v.x, y - v.y);
        }

        inline Vector2Int operator-() const
        {
            return Vector2Int(-x, -y);
        }

        inline void operator+=(const Vector2Int &v)
        {
            x += v.x;
            y += v.y;
        }

        inline void operator-=(const Vector2Int &v)
        {
            x -= v.x;
            y -= v.y;
        }

        inline float magnitude() const
        {
            return std::sqrt(x * x + y * y);
        }

        inline float squareMagnitude() const
        {
            return x * x + y * y;
        }

        inline Vector2Int perpendecular() const
        {
            return Vector2Int(-y, x);
        }
    };

    template <class T>
    class Vector3
    {
    public:
        T x;
        T y;
        T z;

        Vector3() : x(0), y(0), z(0) {}

        Vector3(T x, T y, T z) : x(x), y(y), z(z) {}

        Vector3(const Vector3<T> &vec) : x(vec.x), y(vec.y), z(vec.z) {}

        inline float operator[](int i) const
        {
            if (i == 0)
                return x;
            if (i == 1)
                return y;
            return z;
        }

        inline float &operator[](int i)
        {
            if (i == 0)
                return x;
            if (i == 1)
                return y;
            return z;
        }

        inline void operator+=(const Vector3<T> &v)
        {
            x += v.x;
            y += v.y;
            z += v.z;
        }

        inline Vector3<T> operator+(const Vector3<T> &v) const
        {
            return Vector3<T>(x + v.x, y + v.y, z + v.z);
        }

        inline void operator-=(const Vector3<T> &v)
        {
            x -= v.x;
            y -= v.y;
            z -= v.z;
        }

        inline Vector3<T> operator-(const Vector3<T> &v) const
        {
            return Vector3<T>(x - v.x, y - v.y, z - v.z);
        }

        inline Vector3<T> operator-() const
        {
            return Vector3<T>(-x, -y, -z);
        }

        inline void operator*=(const T v)
        {
            x *= v;
            y *= v;
            z *= v;
        }

        inline Vector3<T> operator*(const T v) const
        {
            return Vector3<T>(x * v, y * v, z * v);
        }
        
        inline Vector3<T> operator/(const T v) const
        {
            return Vector3<T>(x / v, y / v, z / v);
        }

        inline Vector3<T> componentProduct(const Vector3<T> &vector) const
        {
            return Vector3<T>(x * vector.x, y * vector.y, z * vector.z);
        }

        inline void componentProductUpdate(const Vector3<T> &vector)
        {
            x *= vector.x;
            y *= vector.y;
            z *= vector.z;
        }

        inline Vector3<T> vectorProduct(const Vector3<T> &vector) const
        {
            return Vector3<T>(y * vector.z - z * vector.y, z * vector.x - x * vector.z, x * vector.y - y * vector.x);
        }

        inline void operator%=(const Vector3<T> &vector)
        {
            *this = vectorProduct(vector);
        }

        inline Vector3<T> operator%(const Vector3<T> &vector) const
        {
            return Vector3<T>(y * vector.z - z * vector.y, z * vector.x - x * vector.z, x * vector.y - y * vector.x);
        }

        inline float scalarProduct(const Vector3<T> &vector) const
        {
            return x * vector.x + y * vector.y + z * vector.z;
        }

        inline float operator*(const Vector3<T> &vector) const
        {
            return x * vector.x + y * vector.y + z * vector.z;
        }

        inline void addScaledVector(const Vector3<T> &vector, T scale)
        {
            x += vector.x * scale;
            y += vector.y * scale;
            z += vector.z * scale;
        }

        inline T magnitude() const
        {
            return sqrt(x * x + y * y + z * z);
        }

        inline T squareMagnitude() const
        {
            return x * x + y * y + z * z;
        }

        inline void normalise()
        {
            T l = magnitude();
            if (l > 0)
            {
                (*this) *= (1.0f) / l;
            }
        }

        inline void trim(T size)
        {
            if (squareMagnitude() > size * size)
            {
                normalise();
                x *= size;
                y *= size;
                z *= size;
            }
        }

        inline Vector3<T> unit() const
        {
            Vector3 result = *this;
            result.normalise();
            return result;
        }

        inline bool operator==(const Vector3<T> &other) const
        {
            return x == other.x && y == other.y && z == other.z;
        }

        inline bool operator!=(const Vector3<T> &other) const
        {
            return !(*this == other);
        }

        inline bool operator<(const Vector3<T> &other) const
        {
            return x < other.x && y < other.y && z < other.z;
        }

        inline bool operator>(const Vector3<T> &other) const
        {
            return x > other.x && y > other.y && z > other.z;
        }

        inline bool operator<=(const Vector3<T> &other) const
        {
            return x <= other.x && y <= other.y && z <= other.z;
        }

        inline bool operator>=(const Vector3<T> &other) const
        {
            return x >= other.x && y >= other.y && z >= other.z;
        }

        inline void clear()
        {
            x = y = z = 0;
        }

        inline void invert()
        {
            x = -x;
            y = -y;
            z = -z;
        }
    };

    template <class T>
    class Vector4
    {
    public:
        T x;
        T y;
        T z;
        T w;

        Vector4() : x(0), y(0), z(0), w(0) {}

        Vector4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}

        Vector4(const Vector4<T> &vec) : x(vec.x), y(vec.y), z(vec.z), w(vec.w) {}

        inline float operator[](int i) const
        {
            if (i == 0)
                return x;
            if (i == 1)
                return y;
            if (i == 2)
                return z;
            return w;
        }

        inline float &operator[](int i)
        {
            if (i == 0)
                return x;
            if (i == 1)
                return y;
            if (i == 2)
                return z;
            return w;
        }

        inline void operator+=(const Vector4<T> &v)
        {
            x += v.x;
            y += v.y;
            z += v.z;
            w += v.w;
        }

        inline Vector4<T> operator+(const Vector4<T> &v) const
        {
            return Vector4<T>(x + v.x, y + v.y, z + v.z, w + v.w);
        }

        inline void operator-=(const Vector4<T> &v)
        {
            x -= v.x;
            y -= v.y;
            z -= v.z;
            w -= v.w;
        }

        inline Vector4<T> operator-(const Vector4<T> &v) const
        {
            return Vector4<T>(x - v.x, y - v.y, z - v.z, w - v.w);
        }

        inline Vector4<T> operator-() const
        {
            return Vector4<T>(-x, -y, -z, -w);
        }

        inline void operator*=(const T v)
        {
            x *= v;
            y *= v;
            z *= v;
            w *= v;
        }

        inline Vector4<T> operator*(const T v) const
        {
            return Vector4<T>(x * v, y * v, z * v, w * v);
        }

        inline Vector4<T> operator/(const T v) const
        {
            return Vector4<T>(x / v, y / v, z / v, w / v);
        }

        inline Vector4<T> componentProduct(const Vector4<T> &vector) const
        {
            return Vector4<T>(x * vector.x, y * vector.y, z * vector.z, w * vector.w);
        }

        inline void componentProductUpdate(const Vector4<T> &vector)
        {
            x *= vector.x;
            y *= vector.y;
            z *= vector.z;
            w *= vector.w;
        }

        inline float scalarProduct(const Vector4<T> &vector) const
        {
            return x * vector.x + y * vector.y + z * vector.z + w * vector.w;
        }

        inline float operator*(const Vector4<T> &vector) const
        {
            return x * vector.x + y * vector.y + z * vector.z + w * vector.w;
        }

        inline void addScaledVector(const Vector4<T> &vector, T scale)
        {
            x += vector.x * scale;
            y += vector.y * scale;
            z += vector.z * scale;
            w += vector.w * scale;
        }

        inline T magnitude() const
        {
            return sqrt(x * x + y * y + z * z + w * w);
        }

        inline T squareMagnitude() const
        {
            return x * x + y * y + z * z + w * w;
        }

        inline void normalise()
        {
            T l = magnitude();
            if (l > 0)
            {
                (*this) *= (1.0f) / l;
            }
        }

        inline void trim(T size)
        {
            if (squareMagnitude() > size * size)
            {
                normalise();
                x *= size;
                y *= size;
                z *= size;
                w *= size;
            }
        }

        inline Vector4<T> unit() const
        {
            Vector4<T> result = *this;
            result.normalise();
            return result;
        }

        inline bool operator==(const Vector4<T> &other) const
        {
            return x == other.x && y == other.y && z == other.z && w == other.w;
        }

        inline bool operator!=(const Vector4<T> &other) const
        {
            return !(*this == other);
        }

        inline bool operator<(const Vector4<T> &other) const
        {
            return x < other.x && y < other.y && z < other.z && w < other.w;
        }

        inline bool operator>(const Vector4<T> &other) const
        {
            return x > other.x && y > other.y && z > other.z && w > other.w;
        }

        inline bool operator<=(const Vector4<T> &other) const
        {
            return x <= other.x && y <= other.y && z <= other.z && w <= other.w;
        }

        inline bool operator>=(const Vector4<T> &other) const
        {
            return x >= other.x && y >= other.y && z >= other.z && w >= other.w;
        }

        inline void clear()
        {
            x = y = z = w = 0;
        }

        inline void invert()
        {
            x = -x;
            y = -y;
            z = -z;
            w = -w;
        }
    };

    typedef Vector2<float> vec2;
    typedef Vector3<float> vec3;
    typedef Vector4<float> vec4;
}
#endif