#ifndef BASE_H_INCLUDED
#define BASE_H_INCLUDED


#define PI 3.14159265359
#define RADIAN (3.14159265359 / 180.0)
#define DEGREE (180.0 / 3.14159265359)

namespace mth
{
    template <typename T>
    T clamp(T value, T min, T max)
    {
        if(value < min) return min;
        if(value > max) return max;
        return value;
    }
}   

#endif