#ifndef QUATERNION_H_INCLUDED
#define QUATERNION_H_INCLUDED

#include <cmath>
#include <MTH/vectors.h>
#include <MTH/matrix.h>

namespace mth
{
    template <class T>
    class Quaternion
    {
    public:
        T x;
        T y;
        T z;
        T w;

        Quaternion()
        {
            x = 0;
            y = 0;
            z = 0;
            w = 1;
        }

        Quaternion(T x, T y, T z, T w)
            : x(x), y(y), z(z), w(w)
        {
        }

        void setDirect(T xT, T yT, T zT, T angs)
        {
            angs *= RADIAN;
            w = cos(angs / 2);
            x = xT * sin(angs / 2);
            y = yT * sin(angs / 2);
            z = zT * sin(angs / 2);
        }

        void setEuler(T xA, T yA, T zA)
        {
            xA *= RADIAN;
            yA *= RADIAN;
            zA *= RADIAN;

            T cy = cos(zA * 0.5);
            T sy = sin(zA * 0.5);
            T cp = cos(yA * 0.5);
            T sp = sin(yA * 0.5);
            T cr = cos(xA * 0.5);
            T sr = sin(xA * 0.5);

            w = cr * cp * cy + sr * sp * sy;
            x = sr * cp * cy - cr * sp * sy;
            y = cr * sp * cy + sr * cp * sy;
            z = cr * cp * sy - sr * sp * cy;
        }

        void setFromMatrix3(const Matrix3<T> &mat)
        {
            T trace = mat.m[0][0] + mat.m[1][1] + mat.m[2][2];

            if (trace > 0)
            {
                T s = 0.5 / sqrtf(trace + 1.0);
                w = 0.25 / s;
                x = (mat.m[2][1] - mat.m[1][2]) * s;
                y = (mat.m[0][2] - mat.m[2][0]) * s;
                z = (mat.m[1][0] - mat.m[0][1]) * s;
            }
            else
            {
                if (mat.m[0][0] > mat.m[1][1] && mat.m[0][0] > mat.m[2][2])
                {
                    T s = 2.0 * sqrtf(1.0 + mat.m[0][0] - mat.m[1][1] - mat.m[2][2]);
                    w = (mat.m[2][1] - mat.m[1][2]) / s;
                    x = 0.25 * s;
                    y = (mat.m[0][1] + mat.m[1][0]) / s;
                    z = (mat.m[0][2] + mat.m[2][0]) / s;
                }
                else if (mat.m[1][1] > mat.m[2][2])
                {
                    T s = 2.0 * sqrtf(1.0 + mat.m[1][1] - mat.m[0][0] - mat.m[2][2]);
                    w = (mat.m[0][2] - mat.m[2][0]) / s;
                    x = (mat.m[0][1] + mat.m[1][0]) / s;
                    y = 0.25 * s;
                    z = (mat.m[1][2] + mat.m[2][1]) / s;
                }
                else
                {
                    T s = 2.0 * sqrtf(1.0 + mat.m[2][2] - mat.m[0][0] - mat.m[1][1]);
                    w = (mat.m[1][0] - mat.m[0][1]) / s;
                    x = (mat.m[0][2] + mat.m[2][0]) / s;
                    y = (mat.m[1][2] + mat.m[2][1]) / s;
                    z = 0.25 * s;
                }
            }
        }

        mth::Vector3<T> getEuler() const
        {
            mth::Vector3<T> vec(0, 0, 0);

            T sinr_cosp = 2 * (w * x + y * z);
            T cosr_cosp = 1 - 2 * (x * x + y * y);
            vec.x = std::atan2(sinr_cosp, cosr_cosp) * DEGREE;

            T sinp = 2 * (w * y - z * x);
            if (std::abs(sinp) >= 1)
                vec.y = std::copysign(M_PI / 2, sinp) * DEGREE;
            else
                vec.y = std::asin(sinp) * DEGREE;

            T siny_cosp = 2 * (w * z + x * y);
            T cosy_cosp = 1 - 2 * (y * y + z * z);
            vec.z = std::atan2(siny_cosp, cosy_cosp) * DEGREE;

            return vec;
        }

        mth::Matrix3<T> getMatrix3() const 
        {
            return mth::Matrix3<T>(1.0f - 2.0 * (y * y + z * z), 2 * (x * y - w * z), 2 * (x * z + w * y),
                                   2 * (x * y + w * z), 1.0f - 2.0 * (x * x + z * z), 2 * (y * z - w * x),
                                   2 * (x * z - w * y), 2 * (y * z + w * x), 1.0f - 2.0 * (x * x + y * y));
        }

        mth::Matrix4<T> getMatrix() const
        {
            return mth::Matrix4<T>(getMatrix3());
        }
        
        T magnitude() const
        {
            return sqrt(w * w + x * x + y * y + z * z);
        }

        T squareMagnitude() const
        {
            return w * w + x * x + y * y + z * z;
        }

        void normalise()
        {
            T d = x * x + y * y + z * z + w * w;
            if (d == 0)
            {
                return;
            }
            d = ((T)1.0) / sqrt(d);
            x *= d;
            y *= d;
            z *= d;
            w *= d;
        }

        void inverse()
        {
            Quaternion<T> quat = Quaternion(*this);

            quat.x = -quat.x;
            quat.y = -quat.y;
            quat.z = -quat.z;

            float m = this->magnitude();

            w = quat.w / m;
            x = quat.x / m;
            y = quat.y / m;
            z = quat.z / m;
        }

        void operator*=(const Quaternion<T> &multiplier)
        {
            Quaternion<T> q = *this;
            w = q.w * multiplier.w - q.x * multiplier.x -
                q.y * multiplier.y - q.z * multiplier.z;
            x = q.w * multiplier.x + q.x * multiplier.w +
                q.y * multiplier.z - q.z * multiplier.y;
            y = q.w * multiplier.y + q.y * multiplier.w +
                q.z * multiplier.x - q.x * multiplier.z;
            z = q.w * multiplier.z + q.z * multiplier.w +
                q.x * multiplier.y - q.y * multiplier.x;
            
        }

        bool operator==(const Quaternion<T> &q)
        {
            if(this->x == q.x && this->y == q.y && this->z == q.z && this->w == q.w)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        bool operator!=(const Quaternion<T> &q)
        {
            if(this->x == q.x && this->y == q.y && this->z == q.z && this->w == q.w)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        void addScaledVector(const Vector3<T> &vector, T scale)
        {
            Quaternion<T> q(vector.x * scale,
                            vector.y * scale,
                            vector.z * scale, 0);
            q *= *this;
            x += q.x * ((T)0.5);
            y += q.y * ((T)0.5);
            z += q.z * ((T)0.5);
            w += q.w * ((T)0.5);
        }

        void rotateByVector(const Vector3<T> &vector)
        {
            Quaternion<T> q(vector.x, vector.y, vector.z, 0);
            (*this) *= q;
        }

        Vector3<T> rotateVector(const Vector3<T> &vector) const
        {
            return Vector3<T>(2 * (vector.x * (0.5 - (y * y + z * z)) + vector.y * ((x * y - w * z)) + vector.z * (x * z + w * y)),
                              2 * (vector.x * (x * y + w * z) + vector.y * (0.5 - (x * x + z * z)) + vector.z * (y * z - w * x)),
                              2 * (vector.x * (x * z - w * y) + vector.y * (y * z + w * x) + vector.z * (0.5 - (x * x + y * y))));
        }
    };

    typedef Quaternion<float> quat;

}
#endif