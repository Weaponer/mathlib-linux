#ifndef GLGEN_MATRIX_H_INCLUDED
#define GLGEN_MATRIX_H_INCLUDED
#include <MTH/vectors.h>
#include <MTH/matrix.h>
#include <cmath>
#include <MTH/base.h>
#include <MTH/quaternion.h>
namespace mth
{

    static inline mat4 perspective(float fovy, float aspect, float n, float f)
    {
        float q = 1.0f / tan((0.5f * fovy) * RADIAN);
        float A = q / aspect * -1;
        float B = (-(n + f)) / (n - f);
        float C = (2.0f * n * f) / (n - f);

        mat4 result(A, 0, 0, 0,
                    0, q, 0, 0,
                    0, 0, B, C,
                    0, 0, 1, 0);
        return result;
    }

    static inline mat4 inversePerspective(float fovy, float aspect, float n, float f)
    {
        float q = (1.0f / tan((0.5f * fovy) * RADIAN));
        float A = q / (aspect * -1);
        float B = ((n - f)) / (-2.0f * n * f);
        float C = (n + f) / (-2.0f * n * f);

        mat4 result(1 / A, 0, 0, 0,
                    0, 1/ q, 0, 0,
                    0, 0, 0, 1,
                    0, 0, -B, -C);
        return result;
    }

    static inline mat4 ortho(float left, float right, float bottom, float top, float n, float f)
    {
        return mat4(-2.0f / (right - left), 0.0f, 0.0f, -((right + left) / (right - left)),
                    0.0f, 2.0f / (top - bottom), 0.0f, -((top + bottom) / (top - bottom)),
                    0.0f, 0.0f, 2.0f / (f - n), -((f + n) / (f - n)),
                    0, 0, 0, 1.0f);
    }

    static inline mat4 inverseOrtho(float left, float right, float bottom, float top, float n, float f)
    {
        return mat4((right - left) / -2.0f, 0.0f, 0.0f, -((right + left) / 2),
                    0.0f, (top - bottom) / 2.0f, 0.0f, -((top + bottom) / 2),
                    0.0f, 0.0f, (f - n) / 2.0f, ((n + f) / 2),
                    0, 0, 0, 1.0f);
    }

    template <typename T>
    static inline Matrix4<T> translateTranspose(T x, T y, T z)
    {
        return Matrix4<T>(1, 0, 0, 0,
                          0, 1, 0, 0,
                          0, 0, 1, 0,
                          x, y, z, 1);
    }

    template <typename T>
    static inline Matrix4<T> translate(T x, T y, T z)
    {
        return Matrix4<T>(1, 0, 0, x,
                          0, 1, 0, y,
                          0, 0, 1, z,
                          0, 0, 0, 1);
    }

    template <typename T>
    static inline Matrix4<T> scale(T x, T y, T z)
    {
        return Matrix4<T>(x, 0.0f, 0.0f, 0.0f,
                          0.0f, y, 0.0f, 0.0f,
                          0.0f, 0.0f, z, 0.0f,
                          0.0f, 0.0f, 0.0f, 1.0f);
    }

    template <typename T>
    static inline Matrix4<T> scale(const Vector3<T> &v)
    {
        return scale(v.x, v.y, v.z);
    }

    template <typename T>
    static inline Matrix4<T> scale(T x)
    {
        return Matrix4<T>(x, 0.0f, 0.0f, 0.0f,
                          0.0f, x, 0.0f, 0.0f,
                          0.0f, 0.0f, x, 0.0f,
                          0.0f, 0.0f, 0.0f, 1.0f);
    }

    template <typename T>
    static inline Matrix4<T> rotate(T angle, T x, T y, T z)
    {

        const T x2 = x * x;
        const T y2 = y * y;
        const T z2 = z * z;
        float rads = float(angle) * RADIAN;
        const float c = cosf(rads);
        const float s = sinf(rads);
        const float omc = 1.0f - c;

        return Matrix4<T>((x2 * omc + c), (x * y * omc - z * s), (x * z * omc + y * s), 0,
                          (y * x * omc + z * s), (y2 * omc + c), (y * z * omc - x * s), 0,
                          (x * z * omc - y * s), (y * z * omc + x * s), (z2 * omc + c), 0,
                          0, 0, 0, 1);
    }

    template <typename T>
    static inline Matrix4<T> rotateMove(T angle, T x, T y, T z, const mth::Vector3<T> &pos)
    {
        Matrix4<T> mat = rotate(angle, x, y, z);
        mat.m[0][3] = pos.x;
        mat.m[1][3] = pos.y;
        mat.m[2][3] = pos.z;
    }

    template <typename T>
    static inline Matrix4<T> rotateMove(const Quaternion<T> &quat, const mth::Vector3<T> &pos)
    {
        Matrix4<T> mx4 = quat.getMatrix();
        mx4.m[0][3] = pos.x;
        mx4.m[1][3] = pos.y;
        mx4.m[2][3] = pos.z;
        return mx4;
    }

    template <typename T>
    static inline Matrix4<T> rotate(T angle, const Vector3<T> &v)
    {
        return rotate<T>(angle, v.x, v.y, v.z);
    }

    template <typename T>
    static inline Matrix4<T> rotate(T angle_x, T angle_y, T angle_z)
    {
        return rotate(angle_z, 0.0f, 0.0f, 1.0f) *
               rotate(angle_y, 0.0f, 1.0f, 0.0f) *
               rotate(angle_x, 1.0f, 0.0f, 0.0f);
    }

    template <typename T>
    static inline Matrix4<T> TRS(const mth::Vector3<T> &pos, const Quaternion<T> &quat, const mth::Vector3<T> &scale)
    {
        mth::Matrix4<T> res = mth::scale(scale.x, scale.y, scale.z);
        res = quat.getMatrix() * res;
        res = mth::translate(pos.x, pos.y, pos.z) * res;
        return res;
    }

    template <typename T>
    static inline Matrix4<T> inverseTRS(const mth::Matrix4<T> &mat)
    {
        mth::Vector3<T> pos = mth::Vector3<T>(mat.m[0][3], mat.m[1][3], mat.m[2][3]);
        mth::Vector3<T> forward = mth::Vector3<T>(mat.m[0][2], mat.m[1][2], mat.m[2][2]);
	    mth::Vector3<T> up = mth::Vector3<T>(mat.m[0][1], mat.m[1][1], mat.m[2][1]);
	    mth::Vector3<T> right = mth::Vector3<T>(mat.m[0][0], mat.m[1][0], mat.m[2][0]);

        mth::Vector3<T> scale = mth::Vector3<T>(right.magnitude(), up.magnitude(), forward.magnitude());
        right.normalise();
        up.normalise();
        forward.normalise();
        mth::Matrix3<T> rotate;
        rotate.setOrientation(right, up, forward);
        rotate.transpose();
        pos = pos * -1;
        mth::Matrix4<T> mR = mth::Matrix4<T>(rotate);
        mth::Matrix4<T> result = mth::scale(1.0f / scale.x, 1.0f / scale.y, 1.0f / scale.z) * mR * mth::translate(pos.x, pos.y, pos.z);
        return result;
    }

    template <typename T>
    static inline Vector3<T> getPositionInTRS(const mth::Matrix4<T> &mat)
    {
        mth::Vector3<T> pos = mth::Vector3<T>(mat.m[0][3], mat.m[1][3], mat.m[2][3]);
        return pos;
    }

    template <typename T>
    static inline Vector3<T> getScaleInTRS(const mth::Matrix4<T> &mat)
    {
        mth::Vector3<T> forward = mth::Vector3<T>(mat.m[0][2], mat.m[1][2], mat.m[2][2]);
	    mth::Vector3<T> up = mth::Vector3<T>(mat.m[0][1], mat.m[1][1], mat.m[2][1]);
	    mth::Vector3<T> right = mth::Vector3<T>(mat.m[0][0], mat.m[1][0], mat.m[2][0]);

        mth::Vector3<T> scale = mth::Vector3<T>(right.magnitude(), up.magnitude(), forward.magnitude());
        return scale;
    }

    template <typename T>
    static inline Quaternion<T> getRotateInTRS(const mth::Matrix4<T> &mat)
    {
        mth::Vector3<T> forward = mth::Vector3<T>(mat.m[0][2], mat.m[1][2], mat.m[2][2]);
	    mth::Vector3<T> up = mth::Vector3<T>(mat.m[0][1], mat.m[1][1], mat.m[2][1]);
	    mth::Vector3<T> right = mth::Vector3<T>(mat.m[0][0], mat.m[1][0], mat.m[2][0]);
        right.normalise();
        up.normalise();
        forward.normalise();

        mth::Matrix3<T> rotate;
        rotate.setOrientation(right, up, forward);
        Quaternion<T> quat;
        quat.setFromMatrix3(rotate);
        return quat;
    }
}

#endif