#ifndef COLOR_H_INCLUDED
#define COLOR_H_INCLUDED

#include <iostream>
#include <MTH/base.h>
#include <MTH/vectors.h>
#include <math.h>

namespace mth
{
    template <class T>
    class Color
    {
    public:
        T r;
        T g;
        T b;
        T a;

        Color()
        {
            r = g = b = a = 0;
        }

        Color(T r, T g, T b, T a = 1) : r(r), g(g), b(b), a(a)
        {
        }

        Color(const Color<T> &c) : r(c.r), g(c.g), b(c.b), a(c.a)
        {
        }

        inline void normalize()
        {
            if (r < 0)
            {
                r = 0;
            }
            else if (r > 1)
            {
                r = 1;
            }

            if (g < 0)
            {
                g = 0;
            }
            else if (g > 1)
            {
                g = 1;
            }

            if (b < 0)
            {
                b = 0;
            }
            else if (b > 1)
            {
                b = 1;
            }

            if (a < 0)
            {
                a = 0;
            }
            else if (a > 1)
            {
                a = 1;
            }
        }

        inline void setColor(float r, float g, float b, float a = 1)
        {
            this->r = r;
            this->g = g;
            this->b = b;
            this->a = a;
            normalize();
        }

        inline void setColorByte(int r, int g, int b, int a = 255)
        {
            this->r = (float)r / 255.0f;
            this->g = (float)g / 255.0f;
            this->b = (float)b / 255.0f;
            this->a = (float)a / 255.0f;
            normalize();
        }

        inline Vector4<T> getHSVA()
        {
            Vector4<T> out;
            T min = 0;
            T max = 0;
            T delta = 0;

            min = r < g ? r : g;
            min = min < b ? min : b;

            max = r > g ? r : g;
            max = max > b ? max : b;

            out.z = max;
            delta = max - min;
            if (delta < 0.00001f)
            {
                out.y = 0;
                out.x = 0;
                return out;
            }
            if (max > 0.0)
            {
                out.y = (delta / max);
            }
            else
            {
                out.y = 0.0f;
                out.x = NAN;
                return out;
            }
            if (r >= max)
            {
                out.x = (g - b) / delta;
            }
            else if (g >= max)
            {
                out.x = 2.0f + (b - r) / delta;
            }
            else
            {
                out.x = 4.0f + (r - g) / delta;
            }
            out.x *= 60.0f;

            if (out.x < 0.0f)
            {
                out.z += 360.0f;
            }
            out.w = a;
            return out;
        }

        void setHSVA(const Vector4<T>& hsva)
        {
            T hh = 0;
            T p = 0;
            T q = 0;
            T t = 0;
            T ff = 0;
            long i = 0;
            a = hsva.w;
            if (hsva.y <= 0.0f)
            {
                r = hsva.z;
                g = hsva.z;
                b = hsva.z;
                return;
            }
            hh = hsva.x;
            if (hh >= 360.0f)
            {
                hh = 0.0f;
            }
            hh /= 60.0f;
            i = (long)hh;
            ff = hh - i;
            p = hsva.z * (1.0f - hsva.y);
            q = hsva.z * (1.0f - (hsva.y * ff));
            t = hsva.z * (1.0f - (hsva.y * (1.0 - ff)));

            switch (i)
            {
            case 0:
                r = hsva.z;
                g = t;
                b = p;
                break;
            case 1:
                r = q;
                g = hsva.z;
                b = p;
                break;
            case 2:
                r = p;
                g = hsva.z;
                b = t;
                break;
            case 3:
                r = p;
                g = q;
                b = hsva.z;
                break;
            case 4:
                r = t;
                g = p;
                b = hsva.z;
                break;
            case 5:
            default:
                r = hsva.z;
                g = p;
                b = q;
                break;
            }
            normalize();
        }

        void setHSVA(float h, float s, float v, float a)
        {
            setHSVA(mth::vec4(h, s, v, a));
        }

        inline void operator=(const Color<T> &c)
        {
            r = c.r;
            g = c.g;
            b = c.b;
            a = c.a;
        }

        inline bool operator==(const Color<T> &c) const
        {
            return (r == c.r && g == c.g && b == c.b && a == c.a);
        }

        inline bool operator!=(const Color<T> &c) const
        {
            return !(r == c.r && g == c.g && b == c.b && a == c.a);
        }

        inline bool operator>(const Color<T> &c) const
        {
            return (r > c.r && g > c.g && b > c.b && a > c.a);
        }

        inline bool operator<(const Color<T> &c) const
        {
            return (r < c.r && g < c.g && b < c.b && a < c.a);
        }

        inline bool operator>=(const Color<T> &c) const
        {
            return (r >= c.r && g >= c.g && b >= c.b && a >= c.a);
        }

        inline bool operator<=(const Color<T> &c) const
        {
            return (r <= c.r && g <= c.g && b <= c.b && a <= c.a);
        }

        inline Color<T> operator+(const Color<T> &c) const
        {
            return Color<T>(r + c.r, g + c.g, b + c.b, a + c.a);
        }

        inline Color<T> operator-(const Color<T> &c) const
        {
            return Color<T>(r - c.r, g - c.g, b - c.b, a - c.a);
        }

        inline void operator+=(const Color<T> &c)
        {
            r += c.r;
            g += c.g;
            b += c.b;
            a += c.a;
        }

        inline void operator-=(const Color<T> &c)
        {
            r -= c.r;
            g -= c.g;
            b -= c.b;
            a -= c.a;
        }

        inline Color<T> operator*(const T &c) const
        {
            return Color<T>(r * c, g * c, b * c, a * c);
        }

        inline Color<T> operator/(const T &c) const
        {
            return Color<T>(r / c, g / c, b / c, a / c);
        }

        inline void operator*=(const T &c)
        {
            r *= c;
            g *= c;
            b *= c;
            a *= c;
        }

        inline void operator/=(const T &c)
        {
            r /= c;
            g /= c;
            b /= c;
            a /= c;
        }

        inline Color<T> operator*(const Color<T> &c)
        {
            return Color<T>(r * c.r, g * c.g, b * c.b, a * c.a);
        }

        inline void operator*=(const Color<T> &c)
        {
            r *= c.r;
            g *= c.g;
            b *= c.b;
            a *= c.a;
        }
    };

    typedef Color<float> col;
}

#endif