#ifndef BOUND_OBB_H_INCLUDED
#define BOUND_OBB_H_INCLUDED

#include <MTH/matrix.h>
#include <MTH/vectors.h>

namespace mth
{
    template <typename T>
    class BoundOBB
    {
    public:
        Vector3<T> center;
        Matrix3<T> rotate;
        Vector3<T> size;

        BoundOBB() : center(0, 0, 0), rotate(1, 0, 0,
                                             0, 1, 0,
                                             0, 0, 1),
                     size(1, 1, 1)
        {
        }

        BoundOBB(const Vector3<T> &center, const Vector3<T> &size) : center(center), rotate(1, 0, 0,
                                                                                             0, 1, 0,
                                                                                             0, 0, 1),
                                                                      size(size)
        {
        }

        BoundOBB(const Vector3<T> &center, const Matrix3<T> &rotate, const Vector3<T> &size) : center(center), rotate(rotate), size(size)
        {
        }

        BoundOBB(const BoundOBB<T> &bound) : center(bound.center), rotate(bound.rotate), size(bound.size)
        {
        }

        bool checkPointInside(const Vector3<T> &point) const
        {
            Vector3<T> p = point - center;
            p = rotate.transformTranspose(p);

            if (abs(p.x) <= size.x && abs(p.y) <= size.y && abs(p.z) <= size.z)
            {
                return true;
            }
            return false;
        }
    };

    typedef BoundOBB<float> boundOBB;
}
#endif