#ifndef PLANE_H_INCLUDED
#define PLANE_H_INCLUDED

#include "MTH/vectors.h"
#include "MTH/quaternion.h"

namespace mth
{

    template <typename T>
    class Plane
    {
    public:
        T dist;
        Vector3<T> normal;

        Plane() : dist(0), normal(0, 1, 0)
        {
        }

        Plane(const Vector3<T> &origin, const Vector3<T> &normal) : dist(origin * normal), normal(normal)
        {
        }

        Plane(T dist, const Vector3<T> &normal) : dist(dist), normal(normal)
        {
        }

        Plane(T dist, const Quaternion<T> &rotate) : dist(dist), normal(0, 1, 0)
        {
            normal = rotate.rotateVector(normal);
        }

        Plane(const Vector3<T> &A, const Vector3<T> &B, const Vector3<T> &C)
        {
            normal = (B - A) % (C - A);
            normal.normalise();
            dist = normal * A;
        }

        inline bool checkPointOutside(const Vector3<T> &point) const
        {
            return ((point * normal) - dist) > 0;
        }

        inline bool checkPointOnPlane(const Vector3<T> &point) const
        {
            return ((point * normal) - dist) == 0;
        }
    };

    typedef Plane<float> plane;
}
#endif