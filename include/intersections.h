#ifndef INTERSECTIONS_H
#define INTERSECTIONS_H

#include <MTH/boundOBB.h>
#include <MTH/boundAABB.h>
#include <MTH/vectors.h>
#include <MTH/matrix.h>
#include <MTH/plane.h>
#include <cmath>
#include <numeric>
#include <limits>

namespace mth
{
    template <typename T>
    int intersectSegmentPlane(const Vector3<T> &a, const Vector3<T> &b, const Plane<T> &p, float &t, Vector3<T> &q)
    {
        Vector3<T> ab = b - a;
        t = (p.dist - p.normal * a) / (p.normal * ab);

        if(t >= 0.0 && t <= 1.0)
        {
            q = a + ab * t;
            return 1;
        }
        return 0;
    }

    template <typename T>
    T scalarTripleProduct(const Vector3<T> &a, const Vector3<T> &b, const Vector3<T> &c)
    {
        return a * (b % c);
    }

    template <typename T>
    int intersectLineQuad(const Vector3<T> &p, const Vector3<T> &q,
                          const Vector3<T> &a, const Vector3<T> &b, const Vector3<T> &c, const Vector3<T> &d, Vector3<T> &r)
    {
        Vector3<T> pq = q - p;
        Vector3<T> pa = a - p;
        Vector3<T> pb = b - p;
        Vector3<T> pc = c - p;

        Vector3<T> m = pc % pq;
        T v = pa * m;

        if (v >= 0.0)
        {
            T u = -(pb * m);
            if (u < 0.0)
                return 0;
            T w = scalarTripleProduct(pq, pb, pa);
            if (w < 0.0)
                return 0;
            T denom = 1.0 / (u + v + w);
            u *= denom;
            v *= denom;
            w *= denom;
            r = a * u + b * v + c * w;
        }
        else
        {
            Vector3<T> pd = d - p;
            T u = pd * m;
            if (u < 0.0)
                return 0;
            T w = scalarTripleProduct(pq, pa, pd);
            if (w < 0.0)
                return 0;
            v = -v;
            T denom = 1.0 / (u + v + w);
            u *= denom;
            v *= denom;
            w *= denom;
            r = a * u + b * v + c * w;
        }
        return 1;
    }

    template <typename T>
    int intersectRayAABB(const Vector3<T> &p, const Vector3<T> &d, const BoundAABB<T> &a, T &tmin, Vector3<T> &q)
    {
        tmin = 0.0;
        T tmax = std::numeric_limits<T>::max();

        T epsilon = std::numeric_limits<T>::epsilon();
        for (int i = 0; i < 3; i++)
        {
            if (abs(d[i]) < epsilon)
            {
                if (p[i] < a.min[i] || p[i] > a.max[i])
                    return 0;
            }
            else
            {
                T ood = 1.0 / d[i];
                T t1 = (a.min[i] - p[i]) * ood;
                T t2 = (a.max[i] - p[i]) * ood;

                if (t1 > t2)
                {
                    T temp = t2;
                    t2 = t1;
                    t1 = temp;
                }

                if (t1 > tmin)
                    tmin = t1;
                if (t2 < tmax)
                    tmax = t2;
                if (tmin > tmax)
                    return 0;
            }
        }

        q = p + d * tmin;
        return 1;
    }

    template <typename T>
    int intersectRayOBB(const Vector3<T> &p, const Vector3<T> &d, const BoundOBB<T> &o, T &tmin, Vector3<T> &q)
    {
        BoundAABB<T> a = BoundAABB<T>(Vector3<T>(0.0, 0.0, 0.0), o.size);
        Vector3<T> pd = p - o.center;

        int result = intersectRayAABB(o.rotate.transformTranspose(pd), o.rotate.transformTranspose(d), a, tmin, q);
        if (result)
            q = o.rotate.transform(q) + o.center;

        return result;
    }
}

#endif