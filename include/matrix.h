#ifndef MATRIX_H_INCLUDED
#define MATRIX_H_INCLUDED
#include <iostream>
#include <math.h>
#include "MTH/vectors.h"

namespace mth
{
    template <class T>
    class Matrix2
    {
    public:
        T m[4];

        Matrix2()
        {
            m[0] = m[1] = m[2] = m[3] = 0;
        }

        Matrix2(T m0, T m1, T m2, T m3)
        {
            this->m[0] = m0;
            this->m[1] = m1;
            this->m[2] = m2;
            this->m[3] = m3;
        }

        Matrix2(const Matrix2<T> &mt)
        {
            m[0] = mt.m[0];
            m[1] = mt.m[1];
            m[2] = mt.m[2];
            m[3] = mt.m[3];
        }

        inline void setOrientation(const T &radian)
        {
            m[0] = std::cos(radian);
            m[1] = -std::sin(radian);
            m[2] = std::sin(radian);
            m[3] = std::cos(radian);
        }

        inline void setOrientation(const Vector2<T> &up, const Vector2<T> &right)
        {
            m[0] = right.x;
            m[1] = up.x;
            m[2] = right.y;
            m[3] = up.y;
        }

        inline void transpose()
        {
            float tmp = m[1];
            m[1] = m[2];
            m[2] = tmp;
        }

        inline void setSingleMatrix()
        {
            m[0] = 1;
            m[1] = 0;
            m[2] = 0;
            m[3] = 1;
        }

        inline Vector2<T> transform(const Vector2<T> &v) const
        {
            return Vector2<T>(
                v.x * m[0] + v.y * m[1],
                v.x * m[2] + v.y * m[3]);
        }

        inline Vector2<T> transformTranspose(const Vector2<T> &v) const
        {
            return Vector2<T>(
                v.x * m[0] + v.y * m[2],
                v.x * m[1] + v.y * m[3]);
        }

        inline void inverse()
        {
            T det = m[0] * m[3] - m[1] * m[2];
            if (det != 0)
            {
                Matrix2<T> tM;
                tM.m[0] = m[3];
                tM.m[3] = m[0];
                tM.m[1] = -m[1];
                tM.m[2] = -m[2];

                tM *= 1 / det;
                *this = tM;
            }
        }

        inline void inverseRotate()
        {
            transpose();
        }

        inline Vector2<T> getVector(int index) const
        {
            if (index == 0)
            {
                return Vector2<T>(m[0], m[1]);
            }
            else
            {
                return Vector2<T>(m[2], m[3]);
            }
        }

        inline Vector2<T> getVectorTranspose(int index) const
        {
            if (index == 0)
            {
                return Vector2<T>(m[0], m[2]);
            }
            else
            {
                return Vector2<T>(m[1], m[3]);
            }
        }

        inline T getRadian()
        {
            T ang = acos(m[0]);
            if (-asin(m[1]) < 0)
            {
                ang *= -1;
            }
            return ang;
        }

        inline void operator+=(const Matrix2<T> &mt)
        {
            m[0] += mt.m[0];
            m[1] += mt.m[1];
            m[2] += mt.m[2];
            m[3] += mt.m[3];
        }

        inline void operator-=(const Matrix2<T> &mt)
        {
            m[0] -= mt.m[0];
            m[1] -= mt.m[1];
            m[2] -= mt.m[2];
            m[3] -= mt.m[3];
        }

        inline void operator*=(const T &f)
        {
            m[0] *= f;
            m[1] *= f;
            m[2] *= f;
            m[3] *= f;
        }

        inline void operator/=(const T &f)
        {
            m[0] /= f;
            m[1] /= f;
            m[2] /= f;
            m[3] /= f;
        }

        inline void operator*=(const Matrix2<T> &mt)
        {
            Matrix2<T> tmp(m);

            tmp.m[0] = m[0] * mt.m[0] + m[1] * mt.m[2];

            tmp.m[1] = m[0] * mt.m[1] + m[1] * mt.m[3];

            tmp.m[2] = m[2] * mt.m[0] + m[3] * mt.m[2];

            tmp.m[3] = m[2] * mt.m[1] + m[3] * mt.m[3];

            *this = tmp;
        }

        inline Matrix2<T> operator+(const Matrix2<T> &mt) const
        {
            return Matrix2<T>(
                m[0] + mt.m[0], m[1] + mt.m[1],
                m[2] + mt.m[2], m[3] + mt.m[3]);
        }

        inline Matrix2<T> operator-(const Matrix2<T> &mt) const
        {
            return Matrix2<T>(
                m[0] - mt.m[0], m[1] - mt.m[1],
                m[2] - mt.m[2], m[3] - mt.m[3]);
        }

        inline Matrix2<T> operator*(const Matrix2<T> &mt) const
        {
            return Matrix2<T>(
                m[0] * mt.m[0] + m[1] * mt.m[2], m[0] * mt.m[1] + m[1] * mt.m[3],
                m[2] * mt.m[0] + m[3] * mt.m[2], m[2] * mt.m[1] + m[3] * mt.m[3]);
        }

        inline Matrix2<T> operator*(const T &f) const
        {
            return Matrix2<T>(
                m[0] * f, m[1] * f,
                m[2] * f, m[3] * f);
        }

        inline Matrix2<T> operator/(const T &f) const
        {
            return Matrix2<T>(
                m[0] / f, m[1] / f,
                m[2] / f, m[3] / f);
        }
    };

    template <class T>
    class Matrix3
    {
    public:
        T m[3][3];

        Matrix3()
        {
            m[0][0] = m[0][1] = m[0][2] = m[1][0] = m[1][1] = m[1][2] = m[2][0] = m[2][1] = m[2][2] = 0;
        }

        Matrix3(const T m0, const T m1, const T m2, const T m3, const T m4, const T m5, const T m6, const T m7, const T m8)
        {
            this->m[0][0] = m0;
            this->m[0][1] = m1;
            this->m[0][2] = m2;
            this->m[1][0] = m3;
            this->m[1][1] = m4;
            this->m[1][2] = m5;
            this->m[2][0] = m6;
            this->m[2][1] = m7;
            this->m[2][2] = m8;
        }

        Matrix3(const Matrix3<T> &mt)
        {
            m[0][0] = mt.m[0][0];
            m[0][1] = mt.m[0][1];
            m[0][2] = mt.m[0][2];
            m[1][0] = mt.m[1][0];
            m[1][1] = mt.m[1][1];
            m[1][2] = mt.m[1][2];
            m[2][0] = mt.m[2][0];
            m[2][1] = mt.m[2][1];
            m[2][2] = mt.m[2][2];
        }

        inline void transpose()
        {
            float tmp = m[0][1];
            m[0][1] = m[1][0];
            m[1][0] = tmp;
            tmp = m[0][2];
            m[0][2] = m[2][0];
            m[2][0] = tmp;
            tmp = m[1][2];
            m[1][2] = m[2][1];
            m[2][1] = tmp;
        }

        inline void setSingleMatrix()
        {
            m[0][0] = 1;
            m[0][1] = 0;
            m[0][2] = 0;
            m[1][0] = 0;
            m[1][1] = 1;
            m[1][2] = 0;
            m[2][0] = 0;
            m[2][1] = 0;
            m[2][2] = 1;
        }

        inline Vector3<T> transform(const Vector3<T> &v) const
        {
            return Vector3<T>(
                v.x * m[0][0] + v.y * m[0][1] + v.z * m[0][2],
                v.x * m[1][0] + v.y * m[1][1] + v.z * m[1][2],
                v.x * m[2][0] + v.y * m[2][1] + v.z * m[2][2]);
        }

        inline Vector3<T> transformTranspose(const Vector3<T> &v) const
        {
            return Vector3<T>(
                v.x * m[0][0] + v.y * m[1][0] + v.z * m[2][0],
                v.x * m[0][1] + v.y * m[1][1] + v.z * m[2][1],
                v.x * m[0][2] + v.y * m[1][2] + v.z * m[2][2]);
        }

        inline void setScale(T scale)
        {
            m[0][0] = m[0][1] = m[0][2] = m[1][0] = m[1][1] = m[1][2] = m[2][0] = m[2][1] = m[2][2] = 0;
            m[0][0] = scale;
            m[1][1] = scale;
            m[2][2] = scale;
        }

        inline void setOrientation(Vector3<T> right, Vector3<T> up, Vector3<T> forward)
        {

            m[0][0] = right.x;
            m[1][0] = right.y;
            m[2][0] = right.z;
            m[0][1] = up.x;
            m[1][1] = up.y;
            m[2][1] = up.z;
            m[0][2] = forward.x;
            m[1][2] = forward.y;
            m[2][2] = forward.z;
        }

        inline void setInverse(const Matrix3<T> &m)
        {
            T t4 = m.m[0][0] * m.m[1][1];
            T t6 = m.m[0][0] * m.m[1][2];
            T t8 = m.m[0][1] * m.m[1][0];

            T t10 = m.m[0][2] * m.m[1][0];
            T t12 = m.m[0][1] * m.m[2][0];
            T t14 = m.m[0][2] * m.m[2][0];

            T t16 = (t4 * m.m[2][2] - t6 * m.m[2][1] - t8 * m.m[2][2] + t10 * m.m[2][1] + t12 * m.m[1][2] - t14 * m.m[1][1]);

            if (t16 == 0.0)
                return;
            T t17 = 1.0 / t16;

            this->m[0][0] = (m.m[1][1] * m.m[2][2] - m.m[1][2] * m.m[2][1]) * t17;
            this->m[0][1] = -(m.m[0][1] * m.m[2][2] - m.m[0][2] * m.m[2][1]) * t17;
            this->m[0][2] = (m.m[0][1] * m.m[1][2] - m.m[0][2] * m.m[1][1]) * t17;

            this->m[1][0] = -(m.m[1][0] * m.m[2][2] - m.m[1][2] * m.m[2][0]) * t17;
            this->m[1][1] = (m.m[0][0] * m.m[2][2] - t14) * t17;
            this->m[1][2] = -(t6 - t10) * t17;

            this->m[2][0] = (m.m[1][0] * m.m[2][1] - m.m[1][1] * m.m[2][0]) * t17;
            this->m[2][1] = -(m.m[0][0] * m.m[2][1] - t12) * t17;
            this->m[2][2] = (t4 - t8) * t17;
        }

        inline void inverse()
        {
            Matrix3<T> m = *this;
            m.setInverse(*this);
            *this = m;
        }

        inline void inverseRotate()
        {
            transpose();
        }

        inline void inverseScale()
        {
            m[0][0] = 1 / m[0][0];
            m[1][1] = 1 / m[1][1];
            m[2][2] = 1 / m[2][2];
        }

        inline Vector3<T> getVectorTranspose(int index) const
        {
            if (index == 0)
            {
                return Vector3<T>(m[0][0], m[0][1], m[0][2]);
            }
            else if (index == 1)
            {
                return Vector3<T>(m[1][0], m[1][1], m[1][2]);
            }
            else
            {
                return Vector3<T>(m[2][0], m[2][1], m[2][2]);
            }
        }

        inline Vector3<T> getVector(int index) const
        {
            if (index == 0)
            {
                return Vector3<T>(m[0][0], m[1][0], m[2][0]);
            }
            else if (index == 1)
            {
                return Vector3<T>(m[0][1], m[1][1], m[2][1]);
            }
            else
            {
                return Vector3<T>(m[0][2], m[1][2], m[2][2]);
            }
        }

        inline void operator+=(const Matrix3<T> &mt)
        {
            m[0][0] += mt.m[0][0];
            m[0][1] += mt.m[0][1];
            m[0][2] += mt.m[0][2];
            m[1][0] += mt.m[1][0];
            m[1][1] += mt.m[1][1];
            m[1][2] += mt.m[1][2];
            m[2][0] += mt.m[2][0];
            m[2][1] += mt.m[2][1];
            m[2][2] += mt.m[2][2];
        }

        inline void operator-=(const Matrix3<T> &mt)
        {
            m[0][0] -= mt.m[0][0];
            m[0][1] -= mt.m[0][1];
            m[0][2] -= mt.m[0][2];
            m[1][0] -= mt.m[1][0];
            m[1][1] -= mt.m[1][1];
            m[1][2] -= mt.m[1][2];
            m[2][0] -= mt.m[2][0];
            m[2][1] -= mt.m[2][1];
            m[2][2] -= mt.m[2][2];
        }

        inline void operator*=(const T &f)
        {
            m[0][0] *= f;
            m[0][1] *= f;
            m[0][2] *= f;
            m[1][0] *= f;
            m[1][1] *= f;
            m[1][2] *= f;
            m[2][0] *= f;
            m[2][1] *= f;
            m[2][2] *= f;
        }

        inline void operator/=(const T &f)
        {
            m[0][0] /= f;
            m[0][1] /= f;
            m[0][2] /= f;
            m[1][0] /= f;
            m[1][1] /= f;
            m[1][2] /= f;
            m[2][0] /= f;
            m[2][1] /= f;
            m[2][2] /= f;
        }

        inline void operator*=(const Matrix3<T> &mt)
        {
            Matrix3<T> tmp(*this);

            tmp.m[0][0] = m[0][0] * mt.m[0][0] + m[0][1] * mt.m[1][0] + m[0][2] * mt.m[2][0];
            tmp.m[0][1] = m[0][0] * mt.m[0][1] + m[0][1] * mt.m[1][1] + m[0][2] * mt.m[2][1];
            tmp.m[0][2] = m[0][0] * mt.m[0][2] + m[0][1] * mt.m[1][2] + m[0][2] * mt.m[2][2];

            tmp.m[1][0] = m[1][0] * mt.m[0][0] + m[1][1] * mt.m[1][0] + m[1][2] * mt.m[2][0];
            tmp.m[1][1] = m[1][0] * mt.m[0][1] + m[1][1] * mt.m[1][1] + m[1][2] * mt.m[2][1];
            tmp.m[1][2] = m[1][0] * mt.m[0][2] + m[1][1] * mt.m[1][2] + m[1][2] * mt.m[2][2];

            tmp.m[2][0] = m[2][0] * mt.m[0][0] + m[2][1] * mt.m[1][0] + m[2][2] * mt.m[2][0];
            tmp.m[2][1] = m[2][0] * mt.m[0][1] + m[2][1] * mt.m[1][1] + m[2][2] * mt.m[2][1];
            tmp.m[2][2] = m[2][0] * mt.m[0][2] + m[2][1] * mt.m[1][2] + m[2][2] * mt.m[2][2];

            *this = tmp;
        }

        inline Matrix3<T> operator+(const Matrix3<T> &mt) const
        {
            return Matrix3<T>(
                m[0][0] + mt.m[0][0], m[0][1] + mt.m[0][1], m[0][2] + mt.m[0][2],
                m[1][0] + mt.m[1][0], m[1][1] + mt.m[1][1], m[1][2] + mt.m[1][2],
                m[2][0] + mt.m[2][0], m[2][1] + mt.m[2][1], m[2][2] + mt.m[2][2]);
        }

        inline Matrix3<T> operator-(const Matrix3<T> &mt) const
        {
            return Matrix3<T>(
                m[0][0] - mt.m[0][0], m[0][1] - mt.m[0][1], m[0][2] - mt.m[0][2],
                m[1][0] - mt.m[1][0], m[1][1] - mt.m[1][1], m[1][2] - mt.m[1][2],
                m[2][0] - mt.m[2][0], m[2][1] - mt.m[2][1], m[2][2] - mt.m[2][2]);
        }

        inline Matrix3<T> operator*(const Matrix3<T> &mt) const
        {
            Matrix3<T> tmp(*this);

            tmp.m[0][0] = m[0][0] * mt.m[0][0] + m[0][1] * mt.m[1][0] + m[0][2] * mt.m[2][0];
            tmp.m[0][1] = m[0][0] * mt.m[0][1] + m[0][1] * mt.m[1][1] + m[0][2] * mt.m[2][1];
            tmp.m[0][2] = m[0][0] * mt.m[0][2] + m[0][1] * mt.m[1][2] + m[0][2] * mt.m[2][2];

            tmp.m[1][0] = m[1][0] * mt.m[0][0] + m[1][1] * mt.m[1][0] + m[1][2] * mt.m[2][0];
            tmp.m[1][1] = m[1][0] * mt.m[0][1] + m[1][1] * mt.m[1][1] + m[1][2] * mt.m[2][1];
            tmp.m[1][2] = m[1][0] * mt.m[0][2] + m[1][1] * mt.m[1][2] + m[1][2] * mt.m[2][2];

            tmp.m[2][0] = m[2][0] * mt.m[0][0] + m[2][1] * mt.m[1][0] + m[2][2] * mt.m[2][0];
            tmp.m[2][1] = m[2][0] * mt.m[0][1] + m[2][1] * mt.m[1][1] + m[2][2] * mt.m[2][1];
            tmp.m[2][2] = m[2][0] * mt.m[0][2] + m[2][1] * mt.m[1][2] + m[2][2] * mt.m[2][2];

            return tmp;
        }

        inline Matrix3<T> operator*(const T &f) const
        {
            return Matrix3<T>(m[0][0] * f, m[0][1] * f, m[0][2] * f,
                              m[1][0] * f, m[1][1] * f, m[1][2] * f,
                              m[2][0] * f, m[2][1] * f, m[2][2] * f);
        }

        inline Matrix3<T> operator/(const T &f) const
        {
            return Matrix3<T>(m[0][0] / f, m[0][1] / f, m[0][2] / f,
                              m[1][0] / f, m[1][1] / f, m[1][2] / f,
                              m[2][0] / f, m[2][1] / f, m[2][2] / f);
        }
    };

    template <class T>
    class Matrix4
    {
    public:
        T m[4][4];

        Matrix4()
        {
            for (int i = 0; i < 4; i++)
                for (int i2 = 0; i2 < 4; i2++)
                    m[i][i2] = 0;
        }

        Matrix4(const Matrix3<T> &m3)
        {
            m[0][0] = m3.m[0][0];
            m[0][1] = m3.m[0][1];
            m[0][2] = m3.m[0][2];
            m[1][0] = m3.m[1][0];
            m[1][1] = m3.m[1][1];
            m[1][2] = m3.m[1][2];
            m[2][0] = m3.m[2][0];
            m[2][1] = m3.m[2][1];
            m[2][2] = m3.m[2][2];

            m[0][3] = m[1][3] = m[2][3] = m[3][0] = m[3][1] = m[3][2] = 0;
            m[3][3] = 1;
        }

        Matrix4(const T m0, const T m1, const T m2, const T m3, const T m4, const T m5, const T m6, const T m7, const T m8,
                const T m9, const T m10, const T m11, const T m12, const T m13, const T m14, const T m15)
        {
            this->m[0][0] = m0;
            this->m[0][1] = m1;
            this->m[0][2] = m2;
            this->m[0][3] = m3;
            this->m[1][0] = m4;
            this->m[1][1] = m5;
            this->m[1][2] = m6;
            this->m[1][3] = m7;
            this->m[2][0] = m8;
            this->m[2][1] = m9;
            this->m[2][2] = m10;
            this->m[2][3] = m11;
            this->m[3][0] = m12;
            this->m[3][1] = m13;
            this->m[3][2] = m14;
            this->m[3][3] = m15;
        }

        Matrix4(const Matrix4<T> &mt)
        {
            for (int i = 0; i < 4; i++)
                for (int i2 = 0; i2 < 4; i2++)
                    m[i][i2] = mt.m[i][i2];
        }

        inline void setSingleMatrix()
        {
            m[0][0] = 1;
            m[0][1] = 0;
            m[0][2] = 0;
            m[0][3] = 0;
            m[1][0] = 0;
            m[1][1] = 1;
            m[1][2] = 0;
            m[1][3] = 0;
            m[2][0] = 0;
            m[2][1] = 0;
            m[2][2] = 1;
            m[2][3] = 0;
            m[3][0] = 0;
            m[3][1] = 0;
            m[3][2] = 0;
            m[3][3] = 1;
        }

        inline void transpose()
        {
            T tmp = m[0][1];
            m[0][1] = m[1][0];
            m[1][0] = tmp;

            tmp = m[0][2];
            m[0][2] = m[2][0];
            m[2][0] = tmp;

            tmp = m[0][3];
            m[0][3] = m[3][0];
            m[3][0] = tmp;

            tmp = m[1][2];
            m[1][2] = m[2][1];
            m[2][1] = tmp;

            tmp = m[3][1];
            m[3][1] = m[1][3];
            m[1][3] = tmp;

            tmp = m[3][2];
            m[3][2] = m[2][3];
            m[2][3] = tmp;
        }

        inline Vector3<T> transform(const Vector3<T> &v) const
        {
            return Vector3<T>(
                v.x * m[0][0] + v.y * m[0][1] + v.z * m[0][2] + m[0][3],
                v.x * m[1][0] + v.y * m[1][1] + v.z * m[1][2] + m[1][3],
                v.x * m[2][0] + v.y * m[2][1] + v.z * m[2][2] + m[2][3]);
        }

        inline Vector3<T> transformTranspose(const Vector3<T> &v) const
        {
            return Vector3<T>(
                v.x * m[0][0] + v.y * m[1][0] + v.z * m[2][0] + m[3][0],
                v.x * m[0][1] + v.y * m[1][1] + v.z * m[2][1] + m[3][1],
                v.x * m[0][2] + v.y * m[1][2] + v.z * m[2][2] + m[3][2]);
        }


        inline Vector3<T> transformNoMove(const Vector3<T> &v) const
        {
            return Vector3<T>(
                v.x * m[0][0] + v.y * m[0][1] + v.z * m[0][2],
                v.x * m[1][0] + v.y * m[1][1] + v.z * m[1][2],
                v.x * m[2][0] + v.y * m[2][1] + v.z * m[2][2]);
        }

        inline Vector3<T> transformTransposeNoMove(const Vector3<T> &v) const
        {
            return Vector3<T>(
                v.x * m[0][0] + v.y * m[1][0] + v.z * m[2][0],
                v.x * m[0][1] + v.y * m[1][1] + v.z * m[2][1],
                v.x * m[0][2] + v.y * m[1][2] + v.z * m[2][2]);
        }

        inline Vector4<T> transform(const Vector4<T> &v) const
        {
            return Vector4<T>(
                v.x * m[0][0] + v.y * m[0][1] + v.z * m[0][2] + v.w * m[0][3],
                v.x * m[1][0] + v.y * m[1][1] + v.z * m[1][2] + v.w * m[1][3],
                v.x * m[2][0] + v.y * m[2][1] + v.z * m[2][2] + v.w * m[2][3],
                v.x * m[3][0] + v.y * m[3][1] + v.z * m[3][2] + v.w * m[3][3]);
        }

        inline Vector4<T> transformTranspose(const Vector4<T> &v) const
        {
            return Vector4<T>(
                v.x * m[0][0] + v.y * m[1][0] + v.z * m[2][0] + v.w * m[3][0],
                v.x * m[0][1] + v.y * m[1][1] + v.z * m[2][1] + v.w * m[3][1],
                v.x * m[0][2] + v.y * m[1][2] + v.z * m[2][2] + v.w * m[3][2],
                v.x * m[0][3] + v.y * m[1][3] + v.z * m[2][3] + v.w * m[3][3]);
        }

        inline Matrix4<T> operator*(const Matrix4<T> &f) const
        {
            Matrix4<T> res;
            for (int i = 0; i < 4; i++)
            {
                for (int i2 = 0; i2 < 4; i2++)
                {
                    T sum = 0;

                    for (int i3 = 0; i3 < 4; i3++)
                    {
                        sum += m[i][i3] * f.m[i3][i2];
                    }

                    res.m[i][i2] = sum;
                }
            }
            return res;
        }

        inline Matrix4<T> operator*(const T &f) const
        {
            Matrix4<T> mt;
            for (int i = 0; i < 4; i++)
                for (int i2 = 0; i2 < 4; i2++)
                    mt.m[i][i2] = m[i][i2] * f;
            return mt;
        }

        inline Matrix4<T> operator/(const T &f) const
        {
            Matrix4<T> mt;
            for (int i = 0; i < 4; i++)
                for (int i2 = 0; i2 < 4; i2++)
                    mt.m[i][i2] = m[i][i2] / f;
            return mt;
        }

        inline Matrix4<T> operator+(const Matrix4<T> &f) const
        {
            Matrix4<T> mt;
            for (int i = 0; i < 4; i++)
                for (int i2 = 0; i2 < 4; i2++)
                    mt.m[i][i2] = m[i][i2] + f.m[i][i2];
            return mt;
        }

        inline Matrix4<T> operator-(const Matrix4<T> &f) const
        {
            Matrix4<T> mt;
            for (int i = 0; i < 4; i++)
                for (int i2 = 0; i2 < 4; i2++)
                    mt.m[i][i2] = m[i][i2] - f.m[i][i2];
            return mt;
        }

        inline void operator+=(const Matrix4<T> &mt)
        {
            for (int i = 0; i < 4; i++)
                for (int i2 = 0; i2 < 4; i2++)
                    m[i][i2] += mt.m[i][i2];
        }

        inline void operator-=(const Matrix4<T> &mt)
        {
            for (int i = 0; i < 4; i++)
                for (int i2 = 0; i2 < 4; i2++)
                    m[i][i2] -= mt.m[i][i2];
        }

        inline void operator*=(const T &f)
        {
            for (int i = 0; i < 4; i++)
                for (int i2 = 0; i2 < 4; i2++)
                    m[i][i2] *= f;
        }

        inline void operator/=(const T &f)
        {
            for (int i = 0; i < 4; i++)
                for (int i2 = 0; i2 < 4; i2++)
                    m[i][i2] /= f;
        }

        inline void operator*=(const Matrix4<T> &f)
        {
            Matrix4<T> res;
            for (int i = 0; i < 4; i++)
            {
                for (int i2 = 0; i2 < 4; i2++)
                {
                    T sum = 0;

                    for (int i3 = 0; i3 < 4; i3++)
                    {
                        sum += m[i][i3] * f.m[i3][i2];
                    }

                    res.m[i][i2] = sum;
                }
            }
            *this = res;
        }
    };

    template <typename T>
    Matrix3<T> matrix3FromMatrix4(const Matrix4<T> &m)
    {
        return Matrix3<T>(m.m[0][0], m.m[0][1], m.m[0][2],
                          m.m[1][0], m.m[1][1], m.m[1][2],
                          m.m[2][0], m.m[2][1], m.m[2][2]);
    }

    typedef Matrix2<float> mat2;
    typedef Matrix3<float> mat3;
    typedef Matrix4<float> mat4;
}

#endif