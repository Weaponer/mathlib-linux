# C++ Math library.

This math library was compiled from various textbooks and other repositories. It is used to developing simple engines graphic and physics.

## Installation

You can move the include files to your repository. 

Or, if you are on Ubuntu Linux that you can call:

```bash
  sudo ./install_script
```
    